require('fs').watch('reload', () => app.quit())

// to have access to local or global scripts
require(process.cwd() + '/node_modules/benja').paths();

// simple server example
require('http')
  .createServer(require('tiny-cdn').create({}))
  .listen(8080, '0.0.0.0');
  // will respond to :80 too via iptables

const
  url = require('url')
  path = require('path')
  electron = require('electron')
    app = electron.app
    BrowserWindow = electron.BrowserWindow
let mainWindow
// Creation d'une fenêtre avec la fonction "createWindow" de electron
function createWindow() { // création de la fonction "createWindow"
  mainWindow = new BrowserWindow({
    fullscreen: true, // plein écran
    frame: false, // sans bordure
    backgroundColor: '#000000', // arrière plan noir
  })

  // Ajout du document HTML dans la page
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file',
    slashes: true
  }))
}
// Exécution de la fonction "createWindow"
app.on('ready', createWindow)
var xmlhttp = new XMLHttpRequest(); // Soit "XMLHttpRequest" un objet de JavaScript
var url = "https://news.google.com/rss?ned=fr&gl=FR&hl=fr" // "url" l'url cible
var delay = 7000

xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) { // Si tu est OK
    getNews(this); // On execute la fonction getNews
  }
}

// Envoi de notre requête à l'url
xmlhttp.open("GET", url, true);
xmlhttp.send();

function getNews(xml) {
  var xmlDoc = xml.responseXML // On stocke le fichier XML dans une variable
  // Variables en plus pour raccourcir la ligne 20
  var x = xmlDoc.getElementsByTagName("title") // On filtre les titres avec la balise <titre>
  var inc = 2
  var txt = x[inc].childNodes[0].nodeValue
  document.getElementById("news").innerHTML = txt

  function timeoutLoop() {
    var txt = x[inc].childNodes[0].nodeValue
    document.getElementById("news").innerHTML = txt
    if (++inc < 22) {
      setTimeout(timeoutLoop, delay);
    } else {
      inc = 2;
      setTimeout(timeoutLoop, delay)
    }
    console.log(inc)
  }
  setTimeout(timeoutLoop, delay);
}

startTime = function startTime() {
  var today = new Date()
  // var w = today.get
  var h = today.getHours()
  var m = today.getMinutes()
  var s = today.getSeconds()
  h = checkTime(h)
  m = checkTime(m)
  s = checkTime(s)
  document.getElementById('clock').innerHTML =
    h + ":" + m // + ":" + s
  var t = setTimeout(startTime, 500)
}
checkTime = function (i) {
  if (i < 10) { i = "0" + i } // ajouter un zero devant les nombres < 10
  return i
}
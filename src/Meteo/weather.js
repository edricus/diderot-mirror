var icon
var xmlhttp = new XMLHttpRequest()

function updateAPI() {
  url = "http://api.openweathermap.org/data/2.5/weather?q=Paris&APPID=d61b9c42d21e50c5054f2319a8a03280&units=metric"
  sendRequest(url)
  // Création d'une fonction qui va envoyer une requête (xml) à OpenWeatherMap 
  function sendRequest() {
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        // On converti la reponse xml en json
        var data = JSON.parse(xmlhttp.responseText)
        // On récupère les infos utiles contenu dans le JSON
        var weather = {}
        weather.temp = data.main.temp // On récupère la temperature
        weather.icon = data.weather[0].id // On récupère l'ID de l'icone
        weather.city = data.name // On récupère le nom de la ville
        weather.temp = Math.round(weather.temp) // On arrondi la temperature (XX,xx°C de base)
        update(weather)
      }
    }
    xmlhttp.open("GET", url, true)
    xmlhttp.send()
  }
}

function update(weather) {
  console.log(weather.icon)
  switch (weather.icon) {
    case 800: icon.innerHTML = '<img src=src/assets/sunny.gif>'
    break
    case 905: icon.innerHTML = '<img src=src/assets/wind.gif>'
    break
    case 906: icon.innerHTML = '<img src=src/assets/rainy.gif>'
    break
    case 958: icon.innerHTML = '<img src=src/assets/wind.gif>'
    break
    case 959: icon.innerHTML = '<img src=src/assets/wind.gif>'
    break
    default: icon.innerHTML = '<img src=src/assets/sunny.gif>'
  }
  weather.icon >= 200 && weather.icon <= 232 ? icon.innerHTML = '<img src=src/assets/thunderstorm.gif>' : ''
  weather.icon >= 300 && weather.icon <= 310 ? icon.innerHTML = '<img src=src/assets/cloudy.gif>' : ''
  weather.icon >= 311 && weather.icon <= 531 ? icon.innerHTML = '<img src=src/assets/rainy.gif>' : ''
  weather.icon >= 600 && weather.icon <= 622 ? icon.innerHTML = '<img src=src/assets/snow.gif>' : ''
  weather.icon >= 701 && weather.icon <= 781 ? icon.innerHTML = '<img src=src/assets/fog.gif>' : ''
  weather.icon >= 801 && weather.icon <= 804 ? icon.innerHTML = '<img src=src/assets/cloudy.gif>' : ''
  
  temp.innerHTML = weather.temp + "&deg;"
  city.innerHTML = weather.city
}

// On colle nos valeurs dans les <div> de l'index.html
window.onload = function() {
  icon = document.getElementById("icon")
  temp = document.getElementById("temperature")
  city = document.getElementById("ville")

  updateAPI()
}

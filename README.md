<h1 align=center> ★·.·´¯`·.·★ Diderot Mirror ★·.·´¯`·.·★ </h1>

<h4 align=center>Smart-Mirroir sous NodeJS avec reconnaissance vocale et faciale</h4>

![](https://i.imgur.com/CWd679k.gif)

<h2 align=center> Logiciels à installer </h2>

  * <strong>[NodeJs](https://nodejs.org/fr)</strong>    
Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.

  * <strong>[Electron](https://electron.atom.io)</strong>  
Pour créer l'application du Smart-Mirror.  
Ça créé une application web, Discord est une application Web par exemple.  
On pourra acceder aux options dévellopeur en appuyant sur Ctrl+Maj+I une fois l'application ouverte.  
Ça permet de voir les erreurs dans le code, l'éditeur de texte peux pas tout dire.

<h2 align=center>Sublime Text 3</h2>  

Un bon editeur de texte avec pleins de plugins pour faciliter la vie.  
Pour installer des plugins sur Sublime Text il faudra installer PackageControl.  
  Ouvrez SublimeText, appuyer sur Ctrl+Maj+P, recherchez "install" et cliquer sur "Install Package Control"  
  Ensuite toujours avec Ctrl+Maj+P, recherchez "install" et cliquez sur "Package Control: Install Package"  
  Et recherhez les plugins suivants :  

<strong>Pour corriger les fautes</strong>  
* <strong>SublimeLinter</strong>  
Pour installer les plugins qu'il y a plus bas il faudra installer [NodeJS](https://nodejs.org/dist/v8.9.1/node-v8.9.1-x64.msi) sur Windows et utiliser l'invite de commande (<strong>cmd</strong>).  
Pour ouvrir l'invite de commande Windows : Menu Démarrer > Taper "cmd" > Cliquer sur "Invite de commande" ou je sais pas quoi      
  * <strong>jshint</strong>  
Corrige les erreurs de : JavaScript  
  Dans SublimeText : Ctrl+Maj+P > Package Control: Install Package > SublimeLinter-jshint  
  Dans la console Windows (cmd) : npm install -g jshint  
  
  * <strong>csslint</strong>     
Corrige les erreurs de : CSS  
  Dans SublimeText : Ctrl+Maj+P > Package Control: Install Package > SublimeLinter-csslint  
  Dans la console Windows (cmd) : npm install -g csslint

  * <strong>htmlhint</strong>   
Corrige les erreurs de : HTML  
  Dans SublimeText : Ctrl+Maj+P > Package Control: Install Package > SublimeLinter-htmlhint  
  Dans la console Windows (cmd) : npm install -g htmlhint

* <strong>HTML-CSS-JS Prettify</strong>  
CTRL+Maj+H pour organiser le code  
Par defaut, ce plugin fait des [indentations](https://fr.wikipedia.org/wiki/Style_d%27indentation) de 4 espaces.  
Certaines personnes aiment qu'ils y en ai seulement 2, si le code parait trop espacé après l'organisation, on peux modifier ça de cette manière :  
Ctrl+Alt+P > rechercher "Prettify" > Cliquez sur "HTMLPrettify: Set Prettify preferences  
ensuite changer "indent_size": 4 par "indent_size": 2

* <strong>docblockr</strong>    
Ajoute des fonctionnalités pour les commentaires JS et HTML/CSS  
[Plus d'infos](https://packagecontrol.io/packages/DocBlockr)

* <strong>One Dark Material - Theme</strong>  
Un bon theme pour SublimeText, c'est juste pour personnaliser l'apparence donc facultatif  
Pour changer le theme de SublimeText : Ctrl+Alt+P > rechercher "UI" > Cliquez sur "UI: Select Theme" > One Dark  

* <strong>One Dark Color Scheme</strong>  
Le schema de couleur pour le theme One Dark. Ça change les couleurs de la coloration syntaxique. C'est important d'avoir une bonne visibilité sur la coloration syntaxique pour que le code paraisse moins bordelique  
Pour changer le color scheme de SublimeText : Ctrl+Alt+P > rechercher "UI" > Cliquez sur "UI: Select Color Scheme" > One Dark  

